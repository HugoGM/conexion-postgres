package com.conexion.postgres.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.conexion.postgres.UsuarioRepository;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UsuarioTest {
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Test
	public void testInsertarUsuario() {
		
		Usuario usuario = new Usuario();
		usuario.setNombre("Pedro");
		usuario.setApellido("Marmol");
		usuario.setCorreo("pedromarmol@gmail.com");
		usuario.setContrasena("123456");
		
		assertThat(usuarioRepository.save(usuario)).isInstanceOf(Usuario.class); 
		
	}
}
