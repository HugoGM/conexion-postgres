package com.conexion.postgres;

import org.springframework.data.repository.CrudRepository;

import com.conexion.postgres.domain.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

}
